import VMemberCollection from '@/modules/membro/views/VMemberCollection'

export default [
  {
    path: 'members',
    name: 'MEMBRO.MEMBER_COLLECTION.$NAME',
    component: VMemberCollection,
    require: {
      realm: true
    }
  }
]
