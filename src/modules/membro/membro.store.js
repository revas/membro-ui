import types from './membro.store.mutations'

const createMembroStore = ($http) => {
  return {
    namespaced: true,
    state: {
      isWaiting: false,
      membersList: []
    },
    getters: {
      isWaiting: (state) => {
        return state.isWaiting
      },
      membersList: (state) => {
        return state.membersList
      }
    },
    mutations: {
      [types.MEMBRO_CREATE_MEMBER_REQUEST] (state) {
        state.isWaiting = true
      },
      [types.MEMBRO_CREATE_MEMBER_SUCCESS] (state, member) {
        state.isWaiting = false
        state.membersList.push(member)
      },
      [types.MEMBRO_CREATE_MEMBER_FAILURE] (state, execption) {
        state.isWaiting = false
      },
      [types.MEMBRO_GET_MEMBERS_REQUEST] (state) {
        state.isWaiting = true
      },
      [types.MEMBRO_GET_MEMBERS_SUCCESS] (state, members) {
        state.isWaiting = false
        state.membersList = members
      },
      [types.MEMBRO_GET_MEMBERS_FAILURE] (state, execption) {
        state.isWaiting = false
      }
    },
    actions: {
      async createMember ({commit, dispatch, rootGetters}, member) {
        commit(types.MEMBRO_CREATE_MEMBER_REQUEST)
        try {
          const currentRealm = rootGetters['app/currentRealm']
          const response = await $http.post('/members/membro.CreateMembers/', {
            realmsAliases: [currentRealm.alias],
            realmsMembers: [
              {members: [member]}
            ]
          })
          commit(types.MEMBRO_CREATE_MEMBER_SUCCESS, response.data.realmsMembers[0].members[0])
        } catch (e) {
          console.log(e)
          commit(types.MEMBRO_CREATE_MEMBER_FAILURE)
        }
      },
      async getMembers ({commit, rootGetters}) {
        commit(types.MEMBRO_GET_MEMBERS_REQUEST)
        try {
          const currentRealm = rootGetters['app/currentRealm']
          const response = await $http.post('/members/membro.GetMembers/', {
            realmsAliases: [currentRealm.alias]
          })
          commit(types.MEMBRO_GET_MEMBERS_SUCCESS, response.data.realmsMembers[0].members)
        } catch (e) {
          console.log(e)
          commit(types.MEMBRO_GET_MEMBERS_FAILURE)
        }
      }
    }
  }
}

export default createMembroStore
